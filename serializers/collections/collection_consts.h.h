/*
 * collection_consts.h.h
 * This file is part of graphSampler
 *
 * Copyright (C) 2019 - Giacomo Bergami
 *
 * graphSampler is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * graphSampler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with graphSampler. If not, see <http://www.gnu.org/licenses/>.
 */


//
// Created by giacomo on 09/11/17.
//

#ifndef GRAPHSAMPLER_COLLECTION_CONSTS_H_H
#define GRAPHSAMPLER_COLLECTION_CONSTS_H_H

#include "../utils/dovetailing.h"
#include "../commons/globals.h"

#define COLLECTION_ID                                   0
#define COLLECTION_ELEMENT_ID(x)                        ((x)+1)
#define COLLECTION_ELEMENT_CONTENT_ID(x,coll_size)      ((x)+(coll_size+1))

#endif //GRAPHSAMPLER_COLLECTION_CONSTS_H_H
