/*
 * GraphCollectionSerializeSteps.h
 * This file is part of graphSampler
 *
 * Copyright (C) 2019 - Giacomo Bergami
 *
 * graphSampler is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * graphSampler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with graphSampler. If not, see <http://www.gnu.org/licenses/>.
 */


//
// Created by giacomo on 09/11/17.
//

#ifndef GRAPHSAMPLER_SERIALIZEGRAPHCOLLECTION_H
#define GRAPHSAMPLER_SERIALIZEGRAPHCOLLECTION_H

#include <stdio.h>
#include "../commons/structure.h"
#include "../commons/primary_memory/entity_relationship.h"

/**
 * Serializes the entry point to the graph collection
 * @param values            file where to serialize the list of all the elements
 * @param primary_index     Primary index where to memorize the fast element access by id.
 * @param offset            Offset at which the element is starting to write
 * @param coll_size         Size of the overall collection
 * @return
 */
LONG_NUMERIC serializeGraphCollectionEntryPoint(FILE* values, FILE* primary_index, LONG_NUMERIC offset, int coll_size);
LONG_NUMERIC serializeGraphEntryPoint(FILE* values, FILE* primary_index, LONG_NUMERIC offset, ERvertex& coll_size);


#endif //GRAPHSAMPLER_SERIALIZEGRAPHCOLLECTION_H
