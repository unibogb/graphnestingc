/*
 * GraphCollectionSerializeSteps.cpp
 * This file is part of graphSampler
 *
 * Copyright (C) 2019 - Giacomo Bergami
 *
 * graphSampler is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * graphSampler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with graphSampler. If not, see <http://www.gnu.org/licenses/>.
 */

 
//
// Created by giacomo on 09/11/17.
//

#include "collection_consts.h.h"
#include "GraphCollectionSerializeSteps.h"
#include "../../random/RandomGMarkFile.h"
#include "../commons/secondary_memory/primary_index.h"
#include "../commons/primary_memory/entity_relationship.h"
#include "../commons/Serialize.h"

LONG_NUMERIC serializeGraphCollectionEntryPoint(FILE* values, FILE* primary_index, LONG_NUMERIC offset, int coll_size) {
    ERvertex v;
    v.id = COLLECTION_ID;
    v.hash = GRAPH_COLLECTION_HASH;
    for (LONG_NUMERIC i = COLLECTION_ELEMENT_ID(0); i<COLLECTION_ELEMENT_ID(coll_size); i++) {
        v.vertex_containment.emplace_back(i);
    }
    LONG_NUMERIC size = serialize(&v, values);
    struct primary_index pi{v.id, v.hash, offset};
    fwrite(&pi, sizeof(struct primary_index), 1, primary_index);
    return size+offset;
}


LONG_NUMERIC serializeGraphEntryPoint(FILE* values, FILE* primary_index, LONG_NUMERIC offset, ERvertex& v) {
    LONG_NUMERIC size = serialize(&v, values);
    struct primary_index pi{v.id, v.hash, offset};
    fwrite(&pi, sizeof(struct primary_index), 1, primary_index);
    return size+offset;
}
