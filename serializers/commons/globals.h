/*
 * globals.h
 * This file is part of graphSampler
 *
 * Copyright (C) 2019 - Giacomo Bergami
 *
 * graphSampler is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * graphSampler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with graphSampler. If not, see <http://www.gnu.org/licenses/>.
 */


//
// Created by giacomo on 09/11/17.
//

#ifndef GRAPHSAMPLER_GLOBALS_H
#define GRAPHSAMPLER_GLOBALS_H

#include "../utils/dovetailing.h"

#define GRAPH_COLLECTION_HASH       0UL
#define GRAPH_HASH                  1UL
#define ELEMENT_LABEL_HASH(x)       ((unsigned long)((x)+2))

#endif //GRAPHSAMPLER_GLOBALS_H
