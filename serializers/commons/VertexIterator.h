/*
 * VertexIterator.h
 * This file is part of graphSampler
 *
 * Copyright (C) 2019 - Giacomo Bergami
 *
 * graphSampler is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * graphSampler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with graphSampler. If not, see <http://www.gnu.org/licenses/>.
 */


//
// Created by giacomo on 28/07/17.
//

#ifndef PROJECT_VERTEXITERATOR_H
#define PROJECT_VERTEXITERATOR_H

#include "secondary_memory/header.h"

class VertexIterator {
public:
    VertexIterator (header* begin, header* end);

    bool operator!= (const VertexIterator& other) const;

    header* operator*() const;

    const VertexIterator& operator++ ();

    VertexIterator begin () const;

    VertexIterator end () const;



private:
    header* start;
    header* stop;
};


#endif //PROJECT_VERTEXITERATOR_H
