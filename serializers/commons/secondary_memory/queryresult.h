/*
 * queryresult.h
 * This file is part of graphSampler
 *
 * Copyright (C) 2019 - Giacomo Bergami
 *
 * graphSampler is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * graphSampler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with graphSampler. If not, see <http://www.gnu.org/licenses/>.
 */


//
// Created by giacomo on 02/08/17.
//

#ifndef PROJECT_QUERYRESULT_H
#define PROJECT_QUERYRESULT_H

#include "../structure.h"

enum queryresylt_opt {
    IS_VERTEX = 0,
    IS_EDGE = 1,
    IS_EDGE_SRC = 4,
    IS_EDGE_DST = 5,
    IS_VERTEX_CONTAINMENT = 2,
    IS_EDGE_CONTAINMENT = 3
};

struct queryresult {
    LONG_NUMERIC id_returned_element;
    enum queryresylt_opt containment;
    LONG_NUMERIC element;
};

#endif //PROJECT_QUERYRESULT_H
