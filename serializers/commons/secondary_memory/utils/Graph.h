/*
 * Graph.h
 * This file is part of graphSampler
 *
 * Copyright (C) 2019 - Giacomo Bergami
 *
 * graphSampler is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * graphSampler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with graphSampler. If not, see <http://www.gnu.org/licenses/>.
 */


//
// Created by giacomo on 05/11/17.
//

#ifndef NESTING_GRAPH_H
#define NESTING_GRAPH_H

#include <set>
#include <dirent.h>
#include <boost/filesystem/path.hpp>
#include "../header.h"
#include "../../../utils/mmapFile.h"
#include "../primary_index.h"

class Graph {
    unsigned long size = 0;
    unsigned long sizei = 0;
    int fd = 0;
    int fdi = 0;
    std::string handle;

public:
    header* begin = nullptr;
    header* end = nullptr;
    struct primary_index* index = nullptr;

    Graph(char* path);

    unsigned long nVertices();

    bool open();

    bool clos();

    ~Graph();
};


#endif //NESTING_GRAPH_H
