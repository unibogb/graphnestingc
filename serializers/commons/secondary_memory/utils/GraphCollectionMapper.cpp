/*
 * GraphCollectionMapper.cpp
 * This file is part of graphSampler
 *
 * Copyright (C) 2019 - Giacomo Bergami
 *
 * graphSampler is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * graphSampler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with graphSampler. If not, see <http://www.gnu.org/licenses/>.
 */

 
//
// Created by giacomo on 11/11/17.
//

#include "GraphCollectionMapper.h"

GraphCollectionMapper::GraphCollectionMapper(char *operandPath) {
    boost::filesystem::path baseOperandPath{operandPath};
    boost::filesystem::path primary = baseOperandPath / "primary.bin";
    boost::filesystem::path values = baseOperandPath / "values.bin";
    index = (struct primary_index*)mmapFile(primary.string(), &primary_index_size, &primary_index_fd);
    containment = (header*)mmapFile(values.string(), &containment_size, &containment_fd);
}

GraphCollectionMapper::~GraphCollectionMapper() {
    if (containment) {
        munmap(containment,containment_size);
        close(containment_fd);
    }
    if (index) {
        munmap(index,primary_index_size);
        close(primary_index_fd);
    }
}

LONG_NUMERIC GraphCollectionMapper::vertexSetSize() {
    return 0;
}
