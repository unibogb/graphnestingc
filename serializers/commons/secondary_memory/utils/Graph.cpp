/*
 * Graph.cpp
 * This file is part of graphSampler
 *
 * Copyright (C) 2019 - Giacomo Bergami
 *
 * graphSampler is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * graphSampler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with graphSampler. If not, see <http://www.gnu.org/licenses/>.
 */

 
//
// Created by giacomo on 05/11/17.
//

#include "Graph.h"

const boost::filesystem::path VERTICES{"values.bin"};
const boost::filesystem::path PRIMARY{"secondary.bin"};

Graph::Graph(char* path) : handle{path} {
}

unsigned long Graph::nVertices() {
    return sizei / sizeof(struct primary_index);
}

bool Graph::open() {
    if (begin == nullptr && index == nullptr) {
        boost::filesystem::path basic(handle);
        boost::filesystem::path vertices = basic / VERTICES;
        boost::filesystem::path primary = basic / PRIMARY;
        begin = (header*)mmapFile(vertices.c_str(), &size, &fd);
        end = (header*)(((char*)begin) + size);
        index = (struct primary_index*)mmapFile(primary.c_str(), &sizei, &fdi);
        return true;
    } else return false;
}

bool Graph::clos() {
    if (begin != nullptr) {
        munmap(begin, size);
        begin = nullptr;
        close(fd);
        fd = size = 0;

        munmap(index, sizei);
        index = nullptr;
        close(fdi);
        fdi = sizei = 0;
        return true;
    } else return false;
}

Graph::~Graph() {
    clos();
}
