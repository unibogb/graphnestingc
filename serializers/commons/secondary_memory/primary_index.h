/*
 * primary_index.h
 * This file is part of graphSampler
 *
 * Copyright (C) 2019 - Giacomo Bergami
 *
 * graphSampler is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * graphSampler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with graphSampler. If not, see <http://www.gnu.org/licenses/>.
 */


//
// Created by giacomo on 09/11/17.
//
#include "../structure.h"

#ifndef GRAPHSAMPLER_PRIMARY_INDEX_H
#define GRAPHSAMPLER_PRIMARY_INDEX_H

/*
 * Implements the primary index for the GSM structure
 */
struct primary_index {
    primary_index(LONG_NUMERIC id, LONG_NUMERIC hash, LONG_NUMERIC offset);
    primary_index();

    LONG_NUMERIC id;
    LONG_NUMERIC hash;
    LONG_NUMERIC offset;
};

#define vertex_id_index         struct primary_index

struct hashIndex  {
    LONG_NUMERIC hash;
    LONG_NUMERIC offset;
    hashIndex();;
    hashIndex(LONG_NUMERIC hash, LONG_NUMERIC offset);
};


#endif //GRAPHSAMPLER_PRIMARY_INDEX_H
