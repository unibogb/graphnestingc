/*
 * JOINRESULT.h
 * This file is part of graphSampler
 *
 * Copyright (C) 2019 - Giacomo Bergami
 *
 * graphSampler is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * graphSampler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with graphSampler. If not, see <http://www.gnu.org/licenses/>.
 */


//
// Created by giacomo on 11/11/17.
//

#ifndef GRAPHSAMPLER_RESULT_JOIN_H
#define GRAPHSAMPLER_RESULT_JOIN_H

typedef struct {
    unsigned long isVertex;
    unsigned long isEdge;
    unsigned long left;
    unsigned long right;
} JOINRESULT;

#endif //GRAPHSAMPLER_RESULT_JOIN_H
