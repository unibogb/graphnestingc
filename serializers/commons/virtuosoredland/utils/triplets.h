/*
 * triplets.h
 * This file is part of graphSampler
 *
 * Copyright (C) 2019 - Giacomo Bergami
 *
 * graphSampler is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * graphSampler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with graphSampler. If not, see <http://www.gnu.org/licenses/>.
 */


//
// Created by Giacomo Bergami on 10/09/16.
//

#ifndef VIRTUOSOCONNECT_TRIPLETS_H
#define VIRTUOSOCONNECT_TRIPLETS_H

#ifdef __cplusplus
extern "C" {
#endif
int add_triple(librdf_world *world, librdf_node *context, librdf_model* model, const char *s, const char *p, const char *o);
int add_triple_typed(librdf_world *world, librdf_node *context, librdf_model* model, const char *s, const char *p, const char *o);
#ifdef __cplusplus
}
#endif


#endif //VIRTUOSOCONNECT_TRIPLETS_H
