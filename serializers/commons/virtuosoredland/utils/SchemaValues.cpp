/*
 * SchemaValues.cpp
 * This file is part of graphSampler
 *
 * Copyright (C) 2019 - Giacomo Bergami
 *
 * graphSampler is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * graphSampler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with graphSampler. If not, see <http://www.gnu.org/licenses/>.
 */

 
//
// Created by Giacomo Bergami on 11/09/16.
//

#include "SchemaValues.h"

void removeSubstrs(std::string& s,
                   const std::string& p) {
    std::string::size_type n = p.length();

    for (std::string::size_type i = s.find(p);
         i != std::string::npos;
         i = s.find(p))
        s.erase(i, n);
}

int asIntValue(unsigned char *val) {
    std::string validString{(char*)val};
    validString.erase(0, 1);
    removeSubstrs(validString,SPARQL_INT_DEF);
    return std::stoi(validString);
}

std::string asStringValue(unsigned char *val) {
    std::string validString{(char*)val};
    validString.erase(0, 1);
    removeSubstrs(validString,SPARQL_STRING_DEF);
    return validString;
}
