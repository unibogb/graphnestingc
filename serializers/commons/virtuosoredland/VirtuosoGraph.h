/*
 * VirtuosoGraph.h
 * This file is part of graphSampler
 *
 * Copyright (C) 2019 - Giacomo Bergami
 *
 * graphSampler is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * graphSampler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with graphSampler. If not, see <http://www.gnu.org/licenses/>.
 */


//
// Created by Giacomo Bergami on 10/09/16.
//

#ifndef VIRTUOSOCONNECT_VIRTUOSOGRAPH_H
#define VIRTUOSOCONNECT_VIRTUOSOGRAPH_H


#include <string>
#include <librdf.h>
#include "Virtuoso.h"

class VirtuosoGraph {
    Virtuoso* connection;
    librdf_node* context_node;
    std::string iriname;
public:
    VirtuosoGraph();
    VirtuosoGraph(Virtuoso* connection, std::string& graphName);
    VirtuosoGraph(const VirtuosoGraph& cp);
    ~VirtuosoGraph();

    void transaction();
    bool addTriplet(std::string src, std::string pred, std::string dst);
    bool addValueTriplet(std::string src, std::string pred, std::string dst);



    std::string getGraphIRI();

    void commit();

    int countOutgoingEdges(std::string src, std::string prop);
};

#endif //VIRTUOSOCONNECT_VIRTUOSOGRAPH_H
