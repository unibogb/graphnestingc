/*
 * Virtuoso.h
 * This file is part of graphSampler
 *
 * Copyright (C) 2019 - Giacomo Bergami
 *
 * graphSampler is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * graphSampler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with graphSampler. If not, see <http://www.gnu.org/licenses/>.
 */


//
// Created by Giacomo Bergami on 10/09/16.
//

#ifndef VIRTUOSOCONNECT_VIRTUOSO_H
#define VIRTUOSOCONNECT_VIRTUOSO_H

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdarg.h>

#include <unistd.h>

#include <raptor2/raptor.h>
#include <redland.h>

#include "utils/rdf_heuristics.h"
#include "utils/rdf_hash_internal.h"
#include <string>

typedef int  VIRTUOSO_STATUS;
#define STORAGE_FAILURE     1
#define MODEL_FAILURE       2
#define VIRTUOSO_OK         0

class VirtuosoGraph;
class VirtuosoQuery;

class Virtuoso {
    librdf_storage *storage;
    librdf_node* context_node;
    VIRTUOSO_STATUS status = 0;
public:
    Virtuoso();
    Virtuoso(char* bo);
    ~Virtuoso();
    VirtuosoGraph* openGraph(std::string& name);
    VirtuosoQuery* compileQuery(std::string& query);

    librdf_world* world;
    librdf_model* model;
};

#include "VirtuosoGraph.h"
#include "VirtuosoQuery.h"

#endif //VIRTUOSOCONNECT_VIRTUOSO_H
