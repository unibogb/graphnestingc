/*
 * VertexIterator.cpp
 * This file is part of graphSampler
 *
 * Copyright (C) 2019 - Giacomo Bergami
 *
 * graphSampler is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * graphSampler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with graphSampler. If not, see <http://www.gnu.org/licenses/>.
 */

 
//
// Created by giacomo on 28/07/17.
//

#include "VertexIterator.h"

VertexIterator::VertexIterator(header *begin, header *end)
        : start(begin), stop(end)
{ }

bool VertexIterator::operator!=(const VertexIterator &other) const {
    return start != other.start;
}

header *VertexIterator::operator*() const {
    return start;
}

const VertexIterator &VertexIterator::operator++() {
    start = (header*)(((char*)start) + start->length);
    return *this;
}

VertexIterator VertexIterator::begin() const {
    return VertexIterator(start, stop);
}

VertexIterator VertexIterator::end() const {
    return VertexIterator(stop, stop);
}
