/*
 * SerializeVirtuoso.cpp
 * This file is part of graphSampler
 *
 * Copyright (C) 2019 - Giacomo Bergami
 *
 * graphSampler is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * graphSampler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with graphSampler. If not, see <http://www.gnu.org/licenses/>.
 */

 
//
// Created by giacomo on 12/11/17.
//


#include <iostream>
#include <fstream>
#include <ctime>
#include <cmath>
#include "commons/virtuosoredland/Virtuoso.h"
#include "utils/NumberToString.h"
#include "commons/virtuosoredland/test.h"
#include "commons/virtuosoredland/utils/SchemaValues.h"
///media/giacomo/OutputBlank/test-a-graph.txt0.txt

void serializeOperand(std::string basicString, unsigned long iteration) {
    std::string paceStirng = NumberToString<unsigned long int>(iteration);
    basicString = basicString.replace(basicString.find('*'), paceStirng.size(), paceStirng);

    // Starting the connection with virtuoso
    clock_t t = std::clock();
    {
        Virtuoso virt;
        std::string graphPath{"http://test.graph/graph/"+NumberToString<unsigned long>(iteration)+"/"};
        VirtuosoGraph *graph = virt.openGraph(graphPath);
        std::ifstream input(basicString);
        unsigned long long src, dst;
        std::string bogus;
        while (input >> src >> bogus >> dst) {
            graph->addTriplet("http://test.graph/graph/id/"+NumberToString(src),"http://test.graph/graph/edge","http://test.graph/graph/id/"+NumberToString(dst));
        }
        delete graph;
    }
    std::cout << std::pow(10,iteration+1) << "," << ((double)((double)(std::clock() - t))/(CLOCKS_PER_SEC / 1000.0)) << std::endl;
}
//          /etc/virtuoso-opensource-6.1$ sudo virtuoso-t -df

int main(int argc, char** argv) {
    std::string toReplace{argv[1]};
    std::string startIteration{argv[2]};
    unsigned long int min = StringToNumber<unsigned long int>(startIteration);
    std::string endIteration{argv[3]};
    unsigned long int max = StringToNumber<unsigned long int>(endIteration);

    for (unsigned long int i = min; i<=max; i++) {
        serializeOperand(toReplace, i);
    }
}
