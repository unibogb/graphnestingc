/*
 * sampleVirtuoso.h
 * This file is part of graphSampler
 *
 * Copyright (C) 2019 - Giacomo Bergami
 *
 * graphSampler is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * graphSampler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with graphSampler. If not, see <http://www.gnu.org/licenses/>.
 */


//
// Created by Giacomo Bergami on 10/09/16.
//

#ifndef CGRAPH2_SAMPLEVIRTUOSO_H
#define CGRAPH2_SAMPLEVIRTUOSO_H

//#define BASIC_VALUE "http://jackbergus.alwaysdata.net/values/"
#define BASIC_EDGE  "http://jackbergus.alwaysdata.net/edges/"
#define BASIC_PROPERTY "http://jackbergus.alwaysdata.net/property/"

#include <map>
#include <set>
#include <vector>
#include "../../commons/virtuosoredland/VirtuosoGraph.h"

class sampleVirtuoso   {
    Virtuoso* connection;
    VirtuosoGraph handlers;
    clock_t timing;
    bool isLeft;
public:
    ~sampleVirtuoso();
    sampleVirtuoso();
    void addNewVertex(int cc_newcounter, int oldid, unsigned int hash/*, VAEntry_updating* myData*/);
    void addNewEdge(unsigned int src, unsigned int srcHash, unsigned int dst, unsigned int dstHash);
    void serialize(std::string s, int maxId);
    unsigned int getVertexOutSizeByNewId(int cc_newcounter);
    void prepareSerialization(std::string basePath, std::vector<unsigned int> prepareSizes);
};


#endif //CGRAPH2_SAMPLEVIRTUOSO_H
