cmake_minimum_required(VERSION 3.6)
project(graphSampler)

###########
## BOOST ##
###########
set(Boost_USE_STATIC_LIBS ON)
set(Boost_USE_MULTITHREADED ON)
set(Boost_USE_STATIC_RUNTIME ON)
find_package( Boost REQUIRED COMPONENTS serialization system filesystem)
include_directories( ${Boost_INCLUDE_DIRS} )
message(STATUS "Boost_INCLUDE_DIRS: ${Boost_INCLUDE_DIRS}")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${BOOST_CXX_FLAGS} -fno-stack-protector")

####################################
### LibRDF/Redland with Virtuoso ###
####################################
find_package(PkgConfig REQUIRED)
set( ENV{PKG_CONFIG_PATH} "$ENV{PKG_CONFIG_PATH}:/usr/local/lib/pkgconfig" )
LIST(INSERT CMAKE_MODULE_PATH 0 "/opt/local/include/db48")
pkg_search_module(RASQAL2 REQUIRED rasqal>=0.9.33)
pkg_search_module(REDLAND REQUIRED redland)

set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

add_library(graphserializers STATIC

        serializers/commons/primary_memory/element.cpp
        serializers/commons/primary_memory/element.h
        serializers/commons/primary_memory/entity_relationship.cpp
        serializers/commons/primary_memory/entity_relationship.h
        serializers/commons/primary_memory/value.cpp
        serializers/commons/primary_memory/value.h
        serializers/commons/secondary_memory/header.cpp
        serializers/commons/secondary_memory/header.h
        serializers/commons/secondary_memory/primary_index.cpp
        serializers/commons/secondary_memory/primary_index.h
        serializers/commons/secondary_memory/utils/Graph.cpp
        serializers/commons/secondary_memory/utils/Graph.h
        serializers/commons/secondary_memory/utils/GraphCollectionMapper.cpp
        serializers/commons/secondary_memory/utils/GraphCollectionMapper.h
        serializers/commons/globals.h
        serializers/commons/Serialize.cpp
        serializers/commons/Serialize.h
        serializers/commons/structure.cpp
        serializers/commons/structure.h
        serializers/commons/VertexIterator.cpp
        serializers/commons/VertexIterator.h
        serializers/collections/GraphCollectionSerializeSteps.cpp
        serializers/collections/GraphCollectionSerializeSteps.h
        serializers/utils/fwrite.cpp
        serializers/utils/fwrite.h
        serializers/utils/mmapFile.cpp
        serializers/utils/mmapFile.h

        rbtree/Node.h
        rbtree/enums.h
        rbtree/rbtree.h

        numeric/compares.cpp
        numeric/compares.h

        algorithm/sorting/quicksortFile.cpp
        algorithm/sorting/quicksortFile.h
        algorithm/search/binsearch.h
        )

set(SOURCE_FILES main.cpp

        random/RanHash.cpp
        random/RanHash.h
        future/forward/optional.h
        future/unveil.h
        algorithm/sampling/SamplingAlgorithm.cpp
        algorithm/sampling/SamplingAlgorithm.h
        algorithm/sampling/SamplingAlgorithmCache.cpp
        algorithm/sampling/SamplingAlgorithmCache.h
        random/RandomFileLine.cpp
        random/RandomFileLine.h
        random/RandomGMarkFile.cpp
        random/RandomGMarkFile.h
        printVertex.cpp
        algorithm/sampling/argsSampling.cpp
        algorithm/sampling/argsSampling.h)


######################################################################################
## Old application performing the sampling of the data sources
add_executable(graphSampler ${SOURCE_FILES})
target_link_libraries(graphSampler Threads::Threads graphserializers)


######################################################################################
## Example of how to surf efficiently a file system, without pre-allocating the data
add_executable(dirwaler future/dirwalk.cpp)

######################################################################################
## Main application for serializing both operands and Graph collections
set(GMARK_FILE_READER_SOURCES random/RanHash.cpp
                              random/RandomGMarkFile.cpp
                              random/RandomFileLine.cpp
                              numeric/compares.cpp)

#[[set(COMMON_STRUCTURE          serializers/commons/Serialize.cpp
                              serializers/commons/structure.cpp
                              serializers/commons/VertexIterator.cpp
        serializers/collections/GraphCollectionSerializeSteps.cpp)]]

#set(FILE_MMAPPER              serializers/utils/mmapFile.cpp)

add_executable(serialize_graph_collection  serializers/SerializeGraphCollection.cpp
                                            ${GMARK_FILE_READER_SOURCES})
target_link_libraries(serialize_graph_collection ${Boost_LIBRARIES} graphserializers)

######################################################################################
## Checking the collection serializations
add_executable(nestCollections  algorithm/nesting/nestCollections.cpp)
target_link_libraries(nestCollections ${Boost_LIBRARIES} graphserializers)

add_executable(nestPatterns algorithm/nesting/nestEdgePattern.cpp)
target_link_libraries(nestPatterns ${Boost_LIBRARIES} graphserializers)

######################################################################################
## Checking the collection serializations
add_executable(readCollections testSerializer/readCollections.cpp)
target_link_libraries(readCollections ${Boost_LIBRARIES} graphserializers)

add_executable(Ctest C.cpp)
TARGET_LINK_LIBRARIES(Ctest  ${RASQAL2_LIBRARIES} ${REDLAND_LIBRARIES})
target_include_directories(Ctest PUBLIC ${RASQAL2_INCLUDE_DIRS})
set_target_properties(Ctest PROPERTIES INSTALL_RPATH_USE_LINK_PATH TRUE)
target_compile_options(Ctest PUBLIC ${RASQAL2_CFLAGS_OTHER} ${REDLAND_CFLAGS_OTHER})

add_executable(virtuosoSerialize serializers/SerializeVirtuoso.cpp
                                 serializers/commons/virtuosoredland/test.cpp
                                 serializers/commons/virtuosoredland/Virtuoso.cpp
                                 serializers/commons/virtuosoredland/VirtuosoGraph.cpp
                                 serializers/commons/virtuosoredland/VirtuosoQuery.cpp
                                 serializers/commons/virtuosoredland/utils/SchemaValues.cpp
                                 serializers/commons/virtuosoredland/utils/triplets.c)
TARGET_LINK_LIBRARIES(virtuosoSerialize  ${RASQAL2_LIBRARIES} ${REDLAND_LIBRARIES})
target_include_directories(virtuosoSerialize PUBLIC ${RASQAL2_INCLUDE_DIRS})
set_target_properties(virtuosoSerialize PROPERTIES INSTALL_RPATH_USE_LINK_PATH TRUE)
target_compile_options(virtuosoSerialize PUBLIC ${RASQAL2_CFLAGS_OTHER} ${REDLAND_CFLAGS_OTHER})

add_executable(virtuosoBenchmark algorithm/nesting/nestVirtuoso.cpp.cpp
        serializers/commons/virtuosoredland/test.cpp
        serializers/commons/virtuosoredland/Virtuoso.cpp
        serializers/commons/virtuosoredland/VirtuosoGraph.cpp
        serializers/commons/virtuosoredland/VirtuosoQuery.cpp
        serializers/commons/virtuosoredland/utils/SchemaValues.cpp
        serializers/commons/virtuosoredland/utils/triplets.c)
TARGET_LINK_LIBRARIES(virtuosoBenchmark  ${RASQAL2_LIBRARIES} ${REDLAND_LIBRARIES})
target_include_directories(virtuosoBenchmark PUBLIC ${RASQAL2_INCLUDE_DIRS})
set_target_properties(virtuosoBenchmark PROPERTIES INSTALL_RPATH_USE_LINK_PATH TRUE)
target_compile_options(virtuosoBenchmark PUBLIC ${RASQAL2_CFLAGS_OTHER} ${REDLAND_CFLAGS_OTHER})

######################################################################################
## Main application to perform the
#set(JOINER_FILES ${GMARK_FILE_READER_SOURCES} RelationalJoinerNester.cpp )
#add_executable(joiner ${JOINER_FILES})
#target_link_libraries(joiner ${Boost_LIBRARIES})