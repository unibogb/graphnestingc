/*
 * readCollections.cpp
 * This file is part of graphSampler
 *
 * Copyright (C) 2019 - Giacomo Bergami
 *
 * graphSampler is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * graphSampler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with graphSampler. If not, see <http://www.gnu.org/licenses/>.
 */

 

//
// Created by giacomo on 11/11/17.
//

#include <string>
#include <boost/filesystem/path.hpp>
#include <iostream>
#include "../serializers/utils/mmapFile.h"
#include "../serializers/commons/secondary_memory/primary_index.h"
#include "../serializers/commons/secondary_memory/header.h"

/**
 * Class used to load a graph collection. The files are memory mapped
 */
class graph_collection {
    unsigned long primary_index_size;
    int primary_index_fd;
    unsigned long containment_size;
    int containment_fd;

public:
    struct primary_index* index = nullptr;
    header* containment = nullptr;

    graph_collection(char* operandPath) {
        boost::filesystem::path baseOperandPath{operandPath};
        boost::filesystem::path primary = baseOperandPath / "primary.bin";
        boost::filesystem::path values = baseOperandPath / "values.bin";
        index = (struct primary_index*)mmapFile(primary.string(), &primary_index_size, &primary_index_fd);
        containment = (header*)mmapFile(values.string(), &containment_size, &containment_fd);
    }

    ~graph_collection() {
        if (containment) {
            munmap(containment,containment_size);
            close(containment_fd);
        }
        if (index) {
            munmap(index,primary_index_size);
            close(primary_index_fd);
        }
    }
};

/**
 * returns all the graphs stored in the current graph collection as vertices
 */
#define gc_vertex_containment(gc)           vertex_containment((gc).containment)

/**
 * returns the size of all the graphs stored in the current graph collection as vertices
 */
#define gc_vertex_containment_size(gc)      vertex_containment_size((gc).containment)

// helper macro returning the header associated to the current Graph, starting from id=1
#define _gc_get_graph_header(gc,i)          ((header*)(((char*)(gc).containment)+(gc).index[i].offset))

/**
 * If the size of the vertex containment is not zero, it returns the vector of all the id-sorted vertices contained in
 * the current graph
 *
 * i = 1-based count
 */
#define g_vertex_containment(gc,i)          vertex_containment(_gc_get_graph_header(gc,i))

/**
 * Returns the size of the vertex that the current graph contains
 *
 * i = 1-based count
 */
#define g_vertex_containment_size(gc,i)     vertex_containment_size(_gc_get_graph_header(gc,i))

#define g_edge_containment(gc,i)            edge_containment(_gc_get_graph_header(gc,i))

#define g_edge_containment_size(gc,i)       edge_containment_size(_gc_get_graph_header(gc,i))

#define FOREACH_GRAPH_IN_COLLECTION(var,vCont,nCollections,gc) {\
        LONG_NUMERIC *vCont = gc_vertex_containment(gc);\
        LONG_NUMERIC nCollections = gc_vertex_containment_size(gc);\
        for (unsigned long var = 1; i<=nCollections; var++) {

#define END_FOREACH_GRAPH                 }}

//edge_containment_size(_gc_get_graph_header(gc,i))

int main(int argc, char** argv) {

    /*boost::filesystem::path baseOperandPath{argv[1]};
    boost::filesystem::path primary = baseOperandPath / "primary.bin";
    boost::filesystem::path values = baseOperandPath / "values.bin";

    //{
    unsigned long primary_index_size;
    int primary_index_fd;
    // index containing all the offsets
    struct primary_index* index = (struct primary_index*)mmapFile(primary.string(), &primary_index_size, &primary_index_fd);

    //{
    unsigned long containment_size;
    int containment_fd;
    header* containment = (header*)mmapFile(values.string(), &containment_size, &containment_fd);*/
    graph_collection gc{argv[1]};


    //std::cout << vContSize << std::endl;
    FOREACH_GRAPH_IN_COLLECTION(i, vCont, nCollections, gc)
        LONG_NUMERIC containmentId = vCont[i];
        //header* Graph = (header*)(((char*)containment)+index[containmentId].offset);

        LONG_NUMERIC nEdgesInGraph = g_edge_containment_size(gc, i);
        LONG_NUMERIC *edgesVector  = g_edge_containment(gc, i);

        LONG_NUMERIC  nVerticesInGraph = g_vertex_containment_size(gc, i);
        LONG_NUMERIC  *verticesVector  = g_vertex_containment(gc, i);

        for (unsigned long j = 0; j<nEdgesInGraph; j++) {
            std::cout << edgesVector[j] << "\t";
        }
        std::cout << std::endl;
        for (unsigned long j = 0; j<nVerticesInGraph; j++) {
            std::cout << "\t" << verticesVector[j];
        }
        std::cout << std::endl;
    END_FOREACH_GRAPH

    //}
    //munmap(containment,containment_size);
    //close(containment_fd);

    //}
    //munmap(index,primary_index_size);
    //close(primary_index_fd);

}
