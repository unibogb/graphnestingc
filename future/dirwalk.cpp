/* I can be compiled with the command "gcc -o dentls dentls.c" */

#define _GNU_SOURCE
#include <search.h>     /* Defines tree functions */
#include <dirent.h>     /* Defines DT_* constants */
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <string.h>
#include <iostream>



/* Because most filesystems use btree to store dents
 * its very important to perform an in-order removal
 * of the file contents. Performing an 'as-is read' of
 * the contents causes lots of btree rebalancing
 * that has significantly negative effect on unlink performance
 */

/* Tests indicate that performing a ascending order traversal
 * is about 1/3 faster than a descending order traversal */
int compare_fnames(const void *key1, const void *key2) {
    return strcmp((char *)key1, (char *)key2);
}

int countRemoved = 0;

void walk_tree(const void *node, VISIT val, int lvl) {
    int rc = 0;
    switch(val) {
        case leaf:
            //printf("%s\n", *(char **)node);
            rc = unlink(*(char **)node);
            break;
            /* End order is deliberate here as it offers the best btree
             * rebalancing avoidance.
             */
        case endorder:
            //printf("%s\n", *(char **)node);
            rc = unlink(*(char **)node);
            break;
        default:
            return;
            break;
    }

    if (rc < 0) {
        //perror("unlink problem");
        //printf("%s\n", *(char **)node);
        //exit(1);
    } else countRemoved++;

}

void dummy_destroy(void *nil) {
    return;
}

void *tree = NULL;

struct linux_dirent64 {
    ino64_t        d_ino;    /* 64-bit inode number */
    off64_t        d_off;    /* 64-bit offset to next structure */
    unsigned short d_reclen; /* Size of this dirent */
    unsigned char  d_type;   /* File type */
    char           d_name[]; /* Filename (null-terminated) */
};

#define BUFFER_SIZE     1024000

int main(const int argc, const char** argv) {
    int totalfiles = 0;
    int dirfd = -1;
    int offset = 0;
    int bufcount = 0;
    char buffer[BUFFER_SIZE];
    char *d_type;
    struct linux_dirent64 *dent = NULL;
    struct stat dstat;

    /* Test we have a directory path */
    if (argc < 2) {
        fprintf(stderr, "You must supply a valid directory path.\n");
        exit(1);
    }

    const char *path = argv[1];

    /* Standard sanity checking stuff */
    fprintf(stdout, "Sanity check (access)\n");
    if (access(path, R_OK) < 0) {
        perror("Could not access directory");
        exit(1);
    }

    fprintf(stdout, "l-stating\n");
    if (lstat(path, &dstat) < 0) {
        perror("Unable to lstat path");
        exit(1);
    }
    fprintf(stdout, "Result: %d\n",dstat.st_size);

    fprintf(stdout, "is Dir?\n");
    if (!S_ISDIR(dstat.st_mode)) {
        fprintf(stderr, "The path %s is not a directory.\n", path);
        exit(1);
    }

    /* Allocate a buffer of equal size to the directory to store dents */
    /*fprintf(stdout, "malloc-ing\n");
    if ((buffer = malloc(dstat.st_size+10240)) == NULL) {
        perror("malloc failed");
        exit(1);
    }*/

    /* Open the directory */
    fprintf(stdout, "opening the directory\n");
    if ((dirfd = open(path, O_RDONLY)) < 0) {
        perror("Open error");
        exit(1);
    }

    /* Switch directories */
    fchdir(dirfd);

    fprintf(stdout, "Sorting the directories\n");
    unsigned long int count = 0;

    int barWidth = 70;
    unsigned long int hundred = dstat.st_size+10240;
    int prevPos = -1;

    while (bufcount = syscall(SYS_getdents64, dirfd, buffer, BUFFER_SIZE)) {
        offset = 0;

        dent = (struct linux_dirent64 *)buffer;
        while (offset < bufcount) {
            /* Dont print thisdir and parent dir */
            if (!((strcmp(".",dent->d_name) == 0) || (strcmp("..",dent->d_name) == 0))) {
                d_type = (char *)&dent->d_type;
                /* Only print files */
                if (*d_type == DT_REG) {
                    /* Sort all our files into a binary tree */
                    if (!tsearch(dent->d_name, &tree, compare_fnames)) {
                        fprintf(stderr, "Cannot acquire resources for tree!\n");
                        exit(1);
                    }
                    //fprintf(stdout, "\tCurrently scanning: %s\n", dent->d_name);
                    totalfiles++;
                }
            }
            offset += dent->d_reclen;
            dent = (struct linux_dirent64 *)((char*)buffer + offset);
        }
    }
    fprintf(stderr, "Total files: %d\n", totalfiles);
    printf("Performing delete..\n");

    twalk(tree, walk_tree);
    printf("Done: removed %d files\n", countRemoved);
    close(dirfd);
    tdestroy(tree, dummy_destroy);
}