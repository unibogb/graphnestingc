/*
 * nestEdgePattern.cpp
 * This file is part of graphSampler
 *
 * Copyright (C) 2019 - Giacomo Bergami
 *
 * graphSampler is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * graphSampler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with graphSampler. If not, see <http://www.gnu.org/licenses/>.
 */

 
//
// Created by giacomo on 11/11/17.
//

#include <unordered_map>
#include <boost/filesystem/path.hpp>
#include <iostream>
#include <boost/filesystem/operations.hpp>
#include <cmath>
#include <functional>
#include <unordered_set>
#include "../../serializers/utils/NumberToString.h"
#include "../../serializers/commons/secondary_memory/utils/Graph.h"
#include "../../serializers/commons/globals.h"
#include "../../serializers/commons/secondary_memory/queryresult.h"
#include "../../serializers/commons/secondary_memory/JOINRESULT.h"

FILE* benchmark = nullptr;
#define TIME()          (std::clock())
#define EVAL(c)         ((double)((double)(std::clock() - c))/(CLOCKS_PER_SEC / 1000.0))

struct id_dst {
    LONG_NUMERIC edgeId;
    LONG_NUMERIC dst;
    id_dst(LONG_NUMERIC eid, LONG_NUMERIC dst) : edgeId{eid}, dst{dst} {};

    bool operator<(const id_dst &rhs) const {
        if (dst < rhs.dst)
            return true;
        if (rhs.dst < dst)
            return false;
        return edgeId < rhs.edgeId;
    }
};


namespace std {
    template<>
    class hash<id_dst> {
    public:
        size_t operator()(const id_dst &s) const {
            return (s.dst) ^ (s.edgeId << 1);
        }
    };
};

// ELEMENT_LABEL_HASH(0) = PaperId
// ELEMENT_LABEL_HASH(1) = AuthorId
// ELEMENT_LABEL_HASH(3) = edge hash

#define     PAPER_HASH      ELEMENT_LABEL_HASH(0)
#define     AUTHOR_HASH     ELEMENT_LABEL_HASH(1)
#define     EDGE_HASH       ELEMENT_LABEL_HASH(3)

#define     dt(x,y)         ((((unsigned long)x)+((unsigned long)y)+1)*(((unsigned long)x)+((unsigned long)y))/((unsigned long)2))
inline bool operator==(const struct id_dst& lhs, const struct id_dst& rhs){ return (lhs.edgeId == rhs.edgeId) && (lhs.dst == rhs.dst); }

void stepCheck(std::string& operand) {
    Graph graph{(char*)operand.c_str()};
    graph.open();

    std::string cbf = operand + "_containment.bin";
    FILE *containmentFl = fopen(cbf.c_str(),"w+");
    struct queryresult containment;
    containment.containment = queryresylt_opt::IS_VERTEX_CONTAINMENT;

    std::unordered_map<unsigned long, std::unordered_set<id_dst>> eCal;
    header *v = graph.begin;
    for (; v < graph.end; v = vertex_next(v)) {
        if (v->hash == PAPER_HASH)  {
            // Informations concerning the ingoing edges
            LONG_NUMERIC size = vertex_ingoing_edges_len(v);
            //std::cout << v->id << " ~ InGoing edges (" << size << "):" << std::endl;
            struct edges_in_vertices* v3 = vertex_ingoing_edges_vec(v);
            for (int i = 0; i<size; i++) {
                if (v3[i].vertexHash == AUTHOR_HASH && v3[i].edgeHash == EDGE_HASH) {
                    std::pair<std::unordered_map<unsigned long, std::unordered_set<id_dst>>::iterator, bool> node = eCal.insert(std::make_pair(dovetail(1, v3[i].vertexId), std::unordered_set<id_dst>()));
                    //Node<unsigned long, id_dst> *node = eCal.insertKey(dovetail(1,v3[i].vertexId));
                    containment.id_returned_element = dovetail(1,v3[i].vertexId);
                    containment.element = v->id;
                    fwrite(&containment,sizeof(containment),1, containmentFl);

                    for (int j = 0; j<size; j++) {
                        if (v3[j].vertexHash == AUTHOR_HASH && v3[j].edgeHash == EDGE_HASH && j != i) {
                            LONG_NUMERIC edgeId = dovetail(1,dt(v3[i].vertexId, v3[j].vertexId));
                            struct id_dst ij{edgeId, (dovetail(1,v3[j].vertexId))};
                            node.first->second.insert(ij);
                            containment.id_returned_element = edgeId;
                            containment.element = v->id;
                            fwrite(&containment,sizeof(containment),1, containmentFl);
                        }
                    }
                }
            }
        }
    }

    unsigned long long subgraphs = eCal.size();
    std::unordered_map<unsigned long, std::unordered_set<id_dst>>::iterator it = eCal.begin();
    const std::unordered_map<unsigned long, std::unordered_set<id_dst>>::iterator &end = eCal.end();
    std::string adj = operand + "_adjacency.bin";
    FILE* adjacencyFile = fopen64(adj.c_str(),"w");
    while (it != end) {
        JOINRESULT jr;
        jr.isVertex = 1;
        jr.isEdge = 0;
        jr.left = it->first;
        jr.right = 0;
        fwrite(&jr, sizeof(JOINRESULT), 1, adjacencyFile);
        for (const id_dst & is : it->second) {
            subgraphs+=it->second.size();
            JOINRESULT er;
            er.isVertex = 0;
            er.isEdge = 1;
            er.left = is.edgeId;
            er.right = is.dst;
            fwrite(&er, sizeof(JOINRESULT), 1, adjacencyFile);
        }
        it++;
    }
    fclose(adjacencyFile);
}

int main(int argc, char** argv) {
    std::string startIteration{argv[2]};
    unsigned long int min = StringToNumber<unsigned long int>(startIteration);
    std::string endIteration{argv[3]};
    unsigned long int max = StringToNumber<unsigned long int>(endIteration);

    boost::filesystem::path bfp{"benchmark_fastest_algorithm.csv"};
    bool writeHeader = boost::filesystem::exists(bfp);
    benchmark = fopen64("benchmark_fastest_algorithm.csv","wa");
    if (writeHeader) {
        std::string header = "operation,size,msec\n";
        fwrite(header.c_str(), header.size(), 1, benchmark);
    }


    for (unsigned long int iteration = min ; iteration<=max; iteration++) {
        std::cout << "Evaluating step: " << (unsigned long)std::pow(10,iteration+1) << std::endl;
        std::string basicString{argv[1]};
        std::string paceStirng = NumberToString<unsigned long int>(iteration);
        basicString = basicString.replace(basicString.find('*'),paceStirng.size(),paceStirng);
        basicString += "_ser";
        clock_t c = TIME();
        stepCheck(basicString);
        double secs = EVAL(c);
        std::string toWrite = "graphcollection," + NumberToString<unsigned long>((unsigned long)std::pow(10.0,iteration+1)) + "," + NumberToString<double>(secs) + "\n";
        fwrite(toWrite.c_str(),toWrite.size(),1,benchmark);
        fflush(benchmark);
    }
    fclose(benchmark);
}
