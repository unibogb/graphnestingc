/*
 * nestVirtuoso.cpp.cpp
 * This file is part of graphSampler
 *
 * Copyright (C) 2019 - Giacomo Bergami
 *
 * graphSampler is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * graphSampler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with graphSampler. If not, see <http://www.gnu.org/licenses/>.
 */

 
//
// Created by giacomo on 13/11/17.
//

#include <iostream>
#include <cmath>
#include "../../serializers/commons/virtuosoredland/Virtuoso.h"
#include "../../serializers/utils/NumberToString.h"
#include "../../serializers/commons/virtuosoredland/test.h"

std::string queryCompile(int graph) {
    /*return "SELECT * WHERE { "
            "\t\tGRAPH <http://test.graph/graph/" + NumberToString<int>(graph) + "/> { ?s ?p ?o }}";*/
    return "CONSTRUCT {\n"
            "\t?autha ?newedge ?authb.\n"
            "\t?newedge <http://contains.io/nesting> ?paper1.\n"
            "\t?authc <http://test.graph/graph/edge> ?paper2.\n"
            "} WHERE {\n"
            "\t{\n"
            "\t\tGRAPH <http://test.graph/graph/" + NumberToString<int>(graph) + "/> {\n"
            "\t\t\t?autha <http://test.graph/graph/edge> ?paper1.\n"
            "\t\t\t?authb <http://test.graph/graph/edge> ?paper1.\n"
            "\t\t}\n"
            "\t\tFILTER(?autha != ?authb).\n"
            "\t\tBIND(URI(CONCAT(\"http://test.graph/graph/newedge/\",STRAFTER(STR(?autha),\"http://test.graph/graph/id/\"),\"-\",STRAFTER(STR(?authb),\"http://test.graph/graph/id/\"))) as  ?newedge).\n"
            "\t} UNION {\n"
            "\t\tGRAPH <http://test.graph/graph/" + NumberToString<int>(graph) + "/> {\n"
            "\t\t\t?authc <http://test.graph/graph/edge> ?paper2.\n"
            "\t\t}\n"
            "\t\tOPTIONAL {\n"
            "\t\t\t?authd <http://test.graph/graph/edge> ?paper2.\n"
            "\t\t\tFILTER (?authd != ?authc)\n"
            "\t\t}\n"
            "\t\tFILTER(!bound(?authd))\n"
            "\t}\n"
            "}";
    /*return "CONSTRUCT {\n"
            "\t?autha ?newedge ?authb.\n"
            "\t?newedge <http://contains.io/nesting> ?paper1.\n"
            "\t?authc <http://test.graph/graph/edge> ?paper2.\n"
            "} WHERE {\n"
            "\t{\n"
            "\t\tGRAPH <http://test.graph/graph/" + NumberToString<int>(graph) + "/> {\n"
            "\t\t\t?autha <http://test.graph/graph/edge> ?paper1.\n"
            "\t\t\t?authb <http://test.graph/graph/edge> ?paper1.\n"
            "\t\t}\n"
            "\t\tFILTER(?autha != ?authb).\n"
            "\t\tBIND(URI(CONCAT(?autha,\"-\",?authb)) as  ?newedge).\n"
            "\t} UNION {\n"
            "\t\tGRAPH <http://test.graph/graph/"+ NumberToString(graph) + "/> {\n"
            "\t\t\t?authc <http://test.graph/graph/edge> ?paper2.\n"
            "\t\t}\n"
            "\t\tFILTER NOT EXISTS {\n"
            "\t\t\t?authd <http://test.graph/graph/edge> ?paper2.\n"
            "\t\t\tFILTER (?authd != ?autha)\n"
            "\t\t}\n"
            "\t}\n"
            "}";*/
}

int main(int argc, char** argv) {

    std::string startIteration{argv[1]};
    unsigned long int min = StringToNumber<unsigned long int>(startIteration);
    std::string endIteration{argv[2]};
    unsigned long int max = StringToNumber<unsigned long int>(endIteration);

#if 0
    entryPoint(argc,argv);
#else

    for (unsigned long int i = min; i<=max; i++) {
        Virtuoso virt{};
        std::string query{queryCompile(i)};
        std::cout << query << std::endl;
        VirtuosoQuery* q = virt.compileQuery(query);
        clock_t  t = clock();
        if (q->operator()()) {
            t = clock() - t;
            std::cout << (unsigned long)std::pow(10, (i+1)) << " -- " << ((double)t)/(((double)CLOCKS_PER_SEC)/1000.0) << std::endl;
        }
    }
#endif
}
