/*
 * nestCollections.cpp
 * This file is part of graphSampler
 *
 * Copyright (C) 2019 - Giacomo Bergami
 *
 * graphSampler is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * graphSampler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with graphSampler. If not, see <http://www.gnu.org/licenses/>.
 */

 
//
// Created by giacomo on 11/11/17.
//

#include <set>
#include <dirent.h>
#include <boost/filesystem/path.hpp>
#include <iostream>
#include <boost/dynamic_bitset.hpp>
#include <boost/filesystem/operations.hpp>


#include "../../rbtree/rbtree.h"
#include "../../numeric/compares.h"

#include "../../serializers/commons/secondary_memory/header.h"
#include "../../serializers/utils/mmapFile.h"
#include "../../serializers/utils/dovetailing.h"
#include "../../serializers/commons/secondary_memory/queryresult.h"
#include "../../serializers/commons/secondary_memory/JOINRESULT.h"
#include "../../serializers/commons/secondary_memory/utils/GraphCollectionMapper.h"
#include "../../serializers/commons/secondary_memory/utils/Graph.h"
#include "../../serializers/utils/NumberToString.h"

FILE* benchmark = nullptr;
#define TIME()          (std::clock())
#define EVAL(c)         ((double)((double)(std::clock() - c))/(CLOCKS_PER_SEC / 1000.0))

struct id_dst {
    LONG_NUMERIC edgeId;
    LONG_NUMERIC dst;
    id_dst(LONG_NUMERIC eid, LONG_NUMERIC dst) : edgeId{eid}, dst{dst} {};
};

/*struct adjacency {
    LONG_NUMERIC id;
    LONG_NUMERIC isEdge;
    LONG_NUMERIC src;
    LONG_NUMERIC dst;

    adjacency(LONG_NUMERIC vertex) : id{vertex}, isEdge{0}, src{0}, dst{0 } {}
    adjacency(LONG_NUMERIC id, LONG_NUMERIC src, LONG_NUMERIC dst) : id{id}, isEdge{1}, src{src}, dst{dst} {}
};*/

#if 0
void generateGraphAdjacencyContainment(Graph& graph, /*Graph& gv,*/ unsigned long g, unsigned long g_id, unsigned long gv_vi, unsigned long g_vi, FILE* containmentFl) {
    struct containment sc{dovetail(1,g), 0, dovetail(0,g_id)};
    fwrite(&sc, sizeof(struct containment), 1, containmentFl);
    //struct primary_index* gv_v = gv.index;

    header *v = (header*)(((char*)gv.begin)+gv_v[gv_vi].offset);;
    LONG_NUMERIC size = vertex_outgoing_edges_len(v);
    edges_in_vertices *v3 = vertex_outgoing_edges_vec(v);
    std::vector<LONG_NUMERIC> gv_edges;
    for (int i = 0; i<size; i++) {
        gv_edges.emplace_back(v3[i].edgeId);
    }
    std::sort(gv_edges.begin(), gv_edges.end());

    struct primary_index* g_v = graph.index;
    v = (header*)(((char*)graph.begin)+g_v[g_vi].offset);;
    size = vertex_outgoing_edges_len(v);
    v3 = vertex_outgoing_edges_vec(v);
    std::vector<LONG_NUMERIC> g_edges;
    for (int i = 0; i<size; i++) {
        g_edges.emplace_back(v3[i].edgeId);
    }
    std::sort(g_edges.begin(), g_edges.end());

    std::vector<LONG_NUMERIC> v_intersection;
    std::vector<unsigned long>::iterator gvb = gv_edges.begin();
    std::vector<unsigned long>::iterator gve = gv_edges.end();
    std::vector<unsigned long>::iterator gb = g_edges.begin();
    std::vector<unsigned long>::iterator ge = g_edges.end();
    sc.containmendId = 1;
    while (gvb != gve && gb != ge)
        if (*gvb < *gb)
            ++gvb;
        else if (*gb > *gvb)
            ++gb;
        else
        {
            sc.containing = *gvb;
            fwrite(&sc, sizeof(struct containment), 1, containmentFl);
            ++gvb;
            ++gb;
        }
}
#endif

void stepCheck(std::string& basePathForEverything) {

    std::string operand = basePathForEverything + "_ser";
    Graph graph{(char*)operand.c_str()};
    graph.open();

    std::string collections = basePathForEverything + "_operands";
    boost::filesystem::path collectionsPath{collections};

    boost::filesystem::path vertices = collectionsPath / "vertex_multigraphs.txt_ser";
    GraphCollectionMapper calG_V{(char *)vertices.c_str()};

    boost::filesystem::path edges = collectionsPath / "edge_multigraphs.txt_ser";
    GraphCollectionMapper calG_E{(char *)edges.c_str()};

    //std::set<unsigned long> vCal;
    //std::vector<Graph> calG_V;
    //std::vector<Graph> calG_E;
    //unsigned long G_V_N = calG_V.size();
    //unsigned long G_E_N = calG_E.size();
    //RBTree<unsigned long, unsigned long> multimap_v_gv{&compareUL_Int};

    // Initializing the operators
    //std::string graphPath{argv[1]};
    //Graph Graph{graphPath};
    /*Graph.open();
    {
        std::string graphCollectionForVertices{argv[2]};
        graphCollection(graphCollectionForVertices, calG_V);
    }{
        std::string graphCollectionForEdges{argv[3]};
        graphCollection(graphCollectionForEdges,    calG_E);
    }*/

    //Storing all the edges that must be returned in a sorted fashion
    RBTree<unsigned long, id_dst> eCal{&compareUL_Int};

    struct primary_index *g_v = graph.index;    // graph operator: sorting vertices per id
    unsigned long g_vi = 0;                     // pointer to the current graph vertex id
    unsigned long g_N = graph.nVertices();      // size of the vertex collection for the graph
    LONG_NUMERIC  g_id = g_v[g_vi].id;          // current id for the graph vertex

    if (g_N == 0) {
        std::cout << "the output is the same element of the input" << std::endl;
        return;
    }

    std::string cbf = basePathForEverything + "_containment.bin";
    FILE *containmentFl = fopen(cbf.c_str(),"w+");

    // else
    //unsigned long g = 0;
    //unsigned long calG_V_N = 0;
    struct queryresult containment;
    containment.containment = queryresylt_opt::IS_VERTEX_CONTAINMENT;

    // Setting all the vertices that actually contain a nesting
    //boost::dynamic_bitset<> gvbitset(g_N + 1);

    //// STEP 1: defining the vertex containments
    ////         defining which vertices may be returned in the final result
    ////
    // gc_g is the id for the current graph within the vertex collection
    FOREACH_GRAPH_IN_COLLECTION(gc_g, graphs, N, calG_V)
            unsigned long gc_gv_N = g_vertex_containment_size(calG_V, gc_g);
            LONG_NUMERIC  *gc_gv_V  = g_vertex_containment(calG_V, gc_g);
            bool isSet = false;
            LONG_NUMERIC referenceAuthor;
            g_vi = 0;
            g_id = g_v[g_vi].id;          // current id for the graph vertex

            //Scanning all the vertices in gc_g and searching if graph has the desired element
            for (unsigned long gc_gv_vPtr = 0; gc_gv_vPtr <= gc_gv_N; gc_gv_vPtr++) {
                unsigned long gc_gv_v = gc_gv_V[gc_gv_vPtr];

                //std::cout << "graph n°=" << gc_g << " currentVertexId = " << gc_gv_v << " startingFrom= " << g_id << "~" << gc_gv_v << std::endl;

                // While I have more vertices to scan within the graph operand, and while the current graph operand id is
                // less thatn the current graph collection's vertex (from graph), scan the graph operand
                while (g_vi < g_N && g_id+N+1 < gc_gv_v) {
                    g_vi++;
                    g_id = g_v[g_vi].id;
                    //std::cout << " next = " << g_id << std::endl;
                }

                // In case that the previous iteration quit after a vertex match, I must write the containment index
                if (g_id+N+1 == gc_gv_v) {
                    //std::cout << " match? = " << g_id << "~" << g_id+N+1 << " -- " << gc_gv_v << std::endl;
                    // Set that this vertex was preset: it will be used within the edge generation
                    if (!isSet) {
                        isSet = true;
                        //gvbitset[g_id] = true;
                        referenceAuthor = g_id;
                        //std::cout << "\t" << referenceAuthor << "--> ecal= " << dovetail(1,referenceAuthor) << std::endl;
                        eCal.insertKey(dovetail(1,referenceAuthor));
                    } else {
                        //std::cout << "\tREM " << g_id << "--> ecal= " << g_id + N << std::endl;
                        // --- CONTAINMENT FUNCTION
                        // the lower id is the author. The higher ids are the papers
                        containment.id_returned_element = dovetail(1,referenceAuthor);
                        containment.element = g_id;
                        fwrite(&containment,sizeof(struct queryresult), 1, containmentFl);
                    }
                }
            }

        //std::cout << std::endl;
    END_FOREACH_GRAPH


    //// STEP 1: defining the vertex containments
    ////         defining which vertices may be returned in the final result
    ////
    // gc_g is the id for the current graph within the vertex collection

    FOREACH_GRAPH_IN_COLLECTION(gc_g, graphs, N, calG_E)
            unsigned long gc_ge_N = g_vertex_containment_size(calG_E, gc_g);
            LONG_NUMERIC  *gc_ge_E  = g_vertex_containment(calG_E, gc_g);
            bool isSet = false;
            LONG_NUMERIC first = 0;  // g_id without dovetailing
            LONG_NUMERIC second = 0; // g_id without dovetailing
            int count = 0;

            //Scanning all the vertices in gc_g and searching if graph has the desired element
            for (unsigned long gc_ge_ePtr = 0; gc_ge_ePtr < gc_ge_N; gc_ge_ePtr++) {
                g_vi = 0;
                g_id = g_v[g_vi].id;          // current id for the graph vertex
                unsigned long gc_ge_e = gc_ge_E[gc_ge_ePtr];

                // While I have more vertices to scan within the graph operand, and while the current graph operand id is
                // less thatn the current graph collection's vertex (from graph), scan the graph operand
                while (g_vi < g_N && g_id+N+1 < gc_ge_e) {
                    g_vi++;
                    g_id = g_v[g_vi].id;
                }

                // In case that the previous iteration quit after a vertex match, I must write the containment index
                if (g_id+N+1 == gc_ge_e) {

                    // --- CONTAINMENT FUNCTION
                    // The first two elements are the authors, all the others are the papers
                    if (count >= 2) {
                        containment.id_returned_element = dovetail(1,g_graph_id(calG_E, gc_g));
                        containment.element = g_id;
                        fwrite(&containment,sizeof(struct queryresult), 1, containmentFl);
                    }
                    if (count < 2) {
                        if (!count) first = g_id;
                        else if (count == 1) second = g_id;
                        count++;
                        if (count == 2) {
                            Node<unsigned long, id_dst> *hasVertex;
                            //std::cout << "lookup = " << first << "-->" <<dovetail(1, first);
                            if ((hasVertex  = eCal.lookupNode(dovetail(1, first))) != nullptr) {
                                if (eCal.lookupNode(dovetail(1,second))) {
                                    //std::cout << "... Ok";
                                    hasVertex->add(dovetail(1,g_id), dovetail(1,second));
                                }
                            }
                            //std::cout << std::endl;
                        }
                    }
                }
            }
    END_FOREACH_GRAPH
    fclose(containmentFl);

    //std::set<unsigned long>::iterator start = vCal.begin();
    //const std::set<unsigned long>::iterator& end = vCal.end();
    RBTree<unsigned long, id_dst>::rbiterator adjacency = eCal.iterator();


    std::string adj = basePathForEverything + "_adjacency.bin";
    FILE* adjacencyFile = fopen64(adj.c_str(),"w");
    while (adjacency.hasNext()) {
        Node<unsigned long, id_dst> *currentVertexAdjacency = adjacency.disposeWhileIterating();
        JOINRESULT jr;
        jr.isVertex = 1;
        jr.isEdge = 0;
        jr.left = currentVertexAdjacency->key;
        jr.right = 0;
        fwrite(&jr, sizeof(JOINRESULT), 1, adjacencyFile);
        for (struct id_dst &is : currentVertexAdjacency->overflowList) {
            JOINRESULT er;
            er.isVertex = 0;
            er.isEdge = 1;
            er.left = is.edgeId;
            er.right = is.dst;
            fwrite(&er, sizeof(JOINRESULT), 1, adjacencyFile);
        }
    }
    fclose(adjacencyFile);
}

int main(int argc, char** argv) {


    std::string startIteration{argv[2]};
    unsigned long int min = StringToNumber<unsigned long int>(startIteration);
    std::string endIteration{argv[3]};
    unsigned long int max = StringToNumber<unsigned long int>(endIteration);

    boost::filesystem::path bfp{"benchmark_slowest_algorith.csv"};
    bool writeHeader = (boost::filesystem::exists(bfp));
    benchmark = fopen64("benchmark_slowest_algorith.csv","wa");
    if (writeHeader) {
        std::string header = "operation,size,msec\n";
        fwrite(header.c_str(), header.size(), 1, benchmark);
    }

    for (unsigned long int iteration = min ; iteration<=max; iteration++) {
        std::cout << "Evaluating step: " << (unsigned long)std::pow(10,iteration+1) << std::endl;
        std::string basicString{argv[1]};
        std::string paceStirng = NumberToString<unsigned long int>(iteration);
        basicString = basicString.replace(basicString.find('*'),paceStirng.size(),paceStirng);

        clock_t c = TIME();
        stepCheck(basicString);
        double secs = EVAL(c);
        std::string toWrite = "graphcollection," + NumberToString<unsigned long>((unsigned long)std::pow(10.0,iteration+1)) + "," + NumberToString<double>(secs) + "\n";
        fwrite(toWrite.c_str(),toWrite.size(),1,benchmark);
        fflush(benchmark);
    }
    fclose(benchmark);

}
