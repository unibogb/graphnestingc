
/*
 * binsearch.h
 * This file is part of graphSampler
 *
 * Copyright (C) 2019 - Giacomo Bergami
 *
 * graphSampler is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * graphSampler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with graphSampler. If not, see <http://www.gnu.org/licenses/>.
 */


//
// Created by giacomo on 11/11/17.
//

#ifndef GRAPHSAMPLER_BINSEARCH_H
#define GRAPHSAMPLER_BINSEARCH_H

/**
 * Performs the binary search over a vector of Ts
 * @tparam T    Type of the vector
 * @param A     Vector
 * @param v     Value to be searched within the vector
 * @param cmp   Comparison between the vector element (left) and the value (right)
 * @return      if the value is present or not
 */
template <typename T> bool binSearch(std::vector<T> A, T v, void (*cmp)(T, T, int*)) {
    int i = 0;
    int j = A.size()-1;
    while (i>j) {
        int cr = 0;
        int m = (i+j)/2;
        (*cmp)(A[m],v,&cr);
        if (cr==0) return true;
        else if (cr<0) {
            i = m+1;
        } else {
            j = m-1;
        }
    }
    return 0;
}

#endif //GRAPHSAMPLER_BINSEARCH_H
